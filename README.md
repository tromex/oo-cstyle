# Object Oriented C-Style

Experimental repository to study Object Oriented patterns with the C Programming Language.

## Repo structure

For each object model a directory is used, internally a sub-directory is used to experiment with different patterns.

## LICENSE

MIT License

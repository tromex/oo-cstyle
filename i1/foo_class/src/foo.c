
#include <foo.h>
#include <stdlib.h>

struct Foo *foo_new(int value)
{
        struct Foo *obj = malloc(sizeof(struct Foo));
        obj->value = value;
        return obj;
}

void foo_delete(void *obj)
{
        free(obj);
}

int foo_get_value(struct Foo *obj)
{
        return obj->value;
}

void foo_set_value(struct Foo *obj, int value)
{
        obj->value = value;
}


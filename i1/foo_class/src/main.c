
#include <stdio.h>
#include <stdlib.h>
#include <foo.h>

int main()
{
        struct Foo *foo = foo_new(42);
        printf("foo.value = %d\n", foo_get_value(foo));

        foo_set_value(foo, 17);
        printf("foo.value = %d\n", foo_get_value(foo));

        foo_delete(foo);
        return 0;
}

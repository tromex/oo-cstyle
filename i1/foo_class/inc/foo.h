#ifndef FOO_H_
#define FOO_H_

struct Foo
{
        int value;
};

struct Foo *foo_new(int);

void foo_delete(void *);

int foo_get_value(struct Foo *);

void foo_set_value(struct Foo *, int);

#endif
